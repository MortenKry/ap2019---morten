
function setup() {
 createCanvas(windowWidth, windowHeight);
 frameRate (11);
}

function draw() {

  fill(1,80);
  noStroke();
  rect(0, 0, width, height);
  drawThrobber(9);

  // spillerne
  fill(255);
  rect(14, 150, 15, 60);

  fill(255);
  rect(570, 150, 15, 60);

  //midterlinje
  fill(255);
  rect(300, 10, 8, 20);
  fill(255);
  rect(300, 40, 8, 20);
  fill(255);
  rect(300, 70, 8, 20);
  fill(255);
  rect(300, 100, 8, 20);
  fill(255);
  rect(300, 130, 8, 20);
  fill(255);
  rect(300, 160, 8, 20);
  fill(255);
  rect(300, 190, 8, 20);
  fill(255);
  rect(300, 220, 8, 20);
  fill(255);
  rect(300, 250, 8, 20);
  fill(255);
  rect(300, 280, 8, 20);
  fill(255);
  rect(300, 310, 8, 20);
  fill(255);
  rect(300, 340, 8, 20);
  fill(255);
  rect(300, 370, 8, 20);

  //score
  fill(255);
  rect(270, 20, 8, 40);
  fill(255);
  rect(250, 20, 8, 40);
  fill(255);
  rect(330, 20, 8, 40);
  fill(255);
  rect(350, 20, 8, 40);

  noStroke();
  fill(255);
  rect(250, 20, 28, 8);

  noStroke();
  fill(255);
  rect(330, 20, 28, 8);

  noStroke();
  fill(255);
  rect(330, 52, 28, 8);

  noStroke();
  fill(255);
  rect(250, 52, 28, 8);


}

function drawThrobber(num) {
  translate(width/2, height/2);
  let cir = 360/num*(frameCount%num);
  rotate(radians(cir));
  noStroke();
  fill(255);
  rect(35,0,18,18);

}
