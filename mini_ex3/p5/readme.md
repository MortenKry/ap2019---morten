Mini_ex3


![PNG](SS.PNG)



Link: https://gl.githack.com/MortenKry/ap2019---morten/raw/master/mini_ex3/p5/empty-example/index.html


For this mini_ex I made a simple throbber using parts of Winnie's sourcecode and adding it to the context of my first mini_ex. I readjusted parts of it and the initial intention was for the ball of a ping pong game to serve as the throbber in the middle of the screen. This was easily done, but along the way I mistakenly arranged the code in such a way that resulted in not only the ball to circulate as a throbber but the whole game arena. While this was a mistake I ultimately chose to keep it since these surprises are, for me, part of the joy of programming. 

Sometimes a program consists of a lot more than what is initially shown on screen to the viewer/user. 
Though I rarely think about throbbers significance, I truly am grateful for any throbber a program can offer as I know that I myself would get irritated if I was left waiting without knowing the program was loading. That is essentially what a throbber is for; to show that something _is_ happening in the background and that the system did not simply crash. Therefore, you also rarely find websites without any kind of throbber due to it's calming nature and aggravating absence. 
