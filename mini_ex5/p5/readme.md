Mini_ex5_Some_Rain


![PNG](R.PNG)


Run the Program:
https://glcdn.githack.com/MortenKry/ap2019---morten/raw/master/mini_ex5/p5/empty-example/index.html


I had a lot of cathing up to do, so it was nice with a pause-week!


In this mini_ex I looked back to when I tried to create rain. Back then it consisted only of the moving rectangles.
In this mini_ex I experimented a lot more and was really enjoying myself. The sourcecode is easily affected to illustrate all sorts of weather whether it being drizzle, snow, or heavy rain. Coincidentally, it also took me a while longer to finish than I´d liked.


The hardest part was creating the rings-on-the-ground-splash-effect; This took both a decent amount of time and energy. While I am satisfied with the outcome of the program, my initial ambitions weren´t quite met as it showed to be a bit out of my skill-level and too big of a workload at the time. I initially wanted the splash-effect spread out vertically on both the street and houses but settled on a line on a slim street instead. 


The image is from shutterstock, while the rain mp3 is from soundjay.com.


Edit: I later noticed a couple of unnecessary filler lines in the program which I forgot to remove. I may remove them at some point.