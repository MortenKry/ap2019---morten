let img;
let Rain = [];
let sound;
//color
//let water = (20); At this moment, I couldn´t figure out how to make a color variable. This is only grey scale.

function preload(){
img = loadImage('street.jpg');
sound = loadSound('rain.mp3')

}

function setup() {
createCanvas(windowWidth, windowHeight);
//frameRate(20);

sound.loop();

//array of Droplets
for (i = 0; i < 200; i++) {
Rain[i] = new Droplet(random(0, windowWidth), random(0, -3000));
}
}

function draw() {

tint(150, 150, 200);
image(img,0,0,windowWidth,windowHeight);
noTint();

for (i = 0; i < Rain.length; i++) {
Rain[i].drop();
Rain[i].splash();
}

}

function Droplet(x, y) {
this.x = x;
this.y = y;
this.length = random(10,120);
this.r = 0;
this.fade = 200;

this.drop = function() {
noStroke();
fill(130,140,220);

rect(this.x, this.y, 3, this.length);
this.y = this.y + 50
if (this.y > windowHeight) {
}
}

this.splash = function() {
stroke(80,80,150, this.fade);
noFill();
if (this.y > windowHeight-10) {
ellipse(this.x, windowHeight-10, this.r * 4, this.r / 0.8);
this.r++;
this.fade = this.fade - 10;

//rain loop
if (this.fade < 0) {
  this.y = random(0, -100);
  this.length = random(10,80);
  this.r = 0;
  this.fade = 200;
}
}
}
}
