Link to my mini_ex6: https://gitlab.com/MortenKry/ap2019---morten/tree/master/mini_ex6/p5
![PNG of rain](Rain_flowchart.png)


What was troublesome with making a flowchart of my mini_ex6 was that it is a loop so there is no finish, but other than that it is was not very complicated.


Our ideas for the final project was cut down to these two. The first is a throbber with inspiration from E-literature as well. Humourously, it would be a loading bar with text underneath as to show what is being loaded. The text would describe the process of actually writing a final project with everything from finding sources, putting commas, procrastinating, and creating a final draft. At the 99% point the text would say something alonf the lines of "restarting" or "Not good enough. Do better" and the loading bar would reset to 0%. 


![PNG of throbber](Flowchart_throbber.PNG)


Our other idea was a "Packman" ripoff aptly named "Hackman". Differences would include the user playing as Mark Zuckerberg and having to collect people's data with which one would be able to buy upgrades between levels. A potential upgrade could be "user specific advertisements" increasing data collecting or something along those lines.


![PNG of game](Flowchart_game.PNG)



*What might be the possible technical challenges for the two ideas and how are you going to solve them?*


By far the most challenging program of the two would be our game idea “Hackman” (working title). As for the throbber idea, it is fairly simple and we already have a good idea on how the program would work. 


Among the challenges that “Hackman” presents, one which we consider the most potentially troublesome would be getting every part of the program to interact as intended and cooperate with one another. This is mainly due to the overall workload that would go into the creation of the program, which is more than we have dealt with previously. In correlation, we suspect that the amount of barriers, walls, objects and sprites might pose a challenge. The solution should however not be out of reach with the help of the play.library and a workforce of all three group members. 
Other challenges could be gameplay-wise, such as adding changes between levels, a progressive system, and the ability to buy upgrades. 


*How are these flowcharts different from the one of my mini_ex6*
For one, it is easier to create my own flowchart of my mini_ex6 since it is already a made and there are no "what ifs". Meanwhile we had to agree on how our programs should be, not only before but during the making of our flowcharts as new problems and points of discussion arose. 
 

