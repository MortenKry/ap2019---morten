Linkhttps://gl.githack.com/MortenKry/ap2019---morten/raw/master/mini_ex4/p5/empty-example/index.html


![PNG](M.PNG)


Although this mini exercise is fairly simple what concerns its code, I hope the message is somewhat clear. Firstly, I have simply made a single square. The user can interact with the program by clicking whereever on the screen. The clicks are noted in a counter which affects the colour of the central square, working towards a lighter colour on the greyscale. 


Conceptually, the program is meant as a critique of modern digital culture from the point of view of the user. As a user you may not be aware that many of your actions on the internet are tracked and used by a variety of hidden parties for an equally varied and unknown purposes. An example of usage could be how parties will create user specific ads to cater specifically to the user in a way to "personalize their internet experience". Users of the internet will often not be aware of this tendency from the beginning but will become increasingly aware as the algorithm learns more about the user and manages to offer ads that are increasingly relevant to the user's life. I tried to imitate both this increasing awareness and the specified ads in my program; It might take a couple of clicks before the user is aware that something is changing and likewise the more the user interacts with the program the more it changes. 


I have not come with a specific opinion on whether this tracking is positive or negative as I believe it depends on the circumstance. The publishers always press the intention that it is to personalize the user's experience, which may be true in many instances, but can also be argued to be manipulative and sadly alot of tracking happens off-screen in far more omnious ways than ads.
