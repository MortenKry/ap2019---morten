Mini_ex1_Stalemate

![PNG of stalemate](stalemate.PNG)

Link: https://glcdn.githack.com/MortenKry/ap2019---morten/raw/master/mini_ex1/p5/empty-example/index.html


My Program and process

I attempted to recreate a ping-pong setting. I spent most of my time exploring codes, watching tutorials, and pretty much just messing around with the program, which had me stumble upon the “echo”-effect on the ball which I thought was neat although it doesn’t appear in the original game. 
In the tutorial “3.2: The Bouncing Ball – p5.js Tutorial” by The Coding Train on Youtube, I learned how to assign movement to the ball, which inspired me in the direction of recreating a stalemate game of Ping-Pong. The creation of the game components such as the tally, the centerline, and players was enjoyable, as I experimented with placement and size of rectangles.
Although challenges presented itself, e.g. in trying to add sound and change of movement, the struggles made it all the more satisfactory once the program worked as intended. Especially satisfactory was the problem-solving and process of figuring out how stuff worked on your own and combining aspects. 



How it differs from reading and writing 

Although perhaps I should, I don’t proofread what I write normally as closely as I do when coding, granted a small syntax error will break you code and turn it unrenderable. Furthermore, I appreciate the simplicity and directness of code. There are no filler words, and anything redundant or obsolete can be beneficially stripped away. I find this form of effectiveness highly enjoyable.



What coding means to me

I’m intrigued. I’ve known for a while that programming was a significant part of our culture, but only recently have I been presented with the possibilities of code. I’m excited to have this type of power and privilege to explore and learn more on the subject. 
