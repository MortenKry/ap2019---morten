var x=0;
var speed=5;
var lyd;
var bip;

function preload() {
lyd = loadSound("ping.mp3");
bip = loadSound("pong.mp3");

}

function setup() {
// Create the canvas
createCanvas(600, 400);
background(200);

soundFormats('mp3');

}

function draw() {
x = x+speed;

// sej "ekko"-effekt (ved godt det ikke er i Ping Pong, men synes det så nice ud)
fill(1,50);
rect(0, 0, 600, 400);

// spillerne
fill(255);
rect(14, 150, 15, 60);

fill(255);
rect(570, 150, 15, 60);

//midterlinje
fill(255);
rect(300, 10, 8, 20);
fill(255);
rect(300, 40, 8, 20);
fill(255);
rect(300, 70, 8, 20);
fill(255);
rect(300, 100, 8, 20);
fill(255);
rect(300, 130, 8, 20);
fill(255);
rect(300, 160, 8, 20);
fill(255);
rect(300, 190, 8, 20);
fill(255);
rect(300, 220, 8, 20);
fill(255);
rect(300, 250, 8, 20);
fill(255);
rect(300, 280, 8, 20);
fill(255);
rect(300, 310, 8, 20);
fill(255);
rect(300, 340, 8, 20);
fill(255);
rect(300, 370, 8, 20);

//score
fill(255);
rect(270, 20, 8, 40);
fill(255);
rect(250, 20, 8, 40);
fill(255);
rect(330, 20, 8, 40);
fill(255);
rect(350, 20, 8, 40);

noStroke();
fill(255);
rect(250, 20, 28, 8);

noStroke();
fill(255);
rect(330, 20, 28, 8);

noStroke();
fill(255);
rect(330, 52, 28, 8);

noStroke();
fill(255);
rect(250, 52, 28, 8);

// bolden
fill(255)
rect(x, 170, 20,20);

//bevægelse
if(x>width-52) {
speed=-5;
lyd.play();
}

if(x<-width+630) {
speed=5;
bip.play();
}

}
