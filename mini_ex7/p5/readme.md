Mini_ex7

https://gl.githack.com/MortenKry/ap2019---morten/raw/master/mini_ex7/p5/empty-example/index.html

![PNG of boat](boat.PNG)


For this mini_ex I kept it fairly simple as I mainly practiced using the noise() function to which I was introduced to in the Coding Train. Other than that I've just been stylizing the end result. Therefore, the rules of this program are fairly simple and all very much alike.


I considered making the waves and/or the boat into classes so as to have the opportunity to generate slightly unique waves and boats easily, but I also wanted the program to remain un-interactable, so I scrapped that idea.


When looking at the simplicity of the program, I don't think I am breaking any extreme boundaries by saying that I have created it, even as it continuously generates new patterns. On this topic I agree with Marius Watz; a generative program is limited to the capabilities of its creator and can consequently never achieve complete automatism. 


It it however also important to achknowledge the work that went prior to my own novice meddling with p5.js and how I am only given the carefully crafted resources, created by someone else, and that their resources, in turn, was made by someone else entirely. Everyone is standing on top of previous generations' shoulders.