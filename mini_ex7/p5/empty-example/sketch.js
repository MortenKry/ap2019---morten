
let inc =0.003; //bølgehastighed
let xoloff = 1000;
let start=0;
let xboat=2000;
function setup() {
createCanvas(windowWidth, windowHeight)}

function draw() {
background(191, 209, 229);


//here comes the sun, little darling...
fill(255,255,146,180)
let xol = map(noise(xoloff),0,1,220,400);
ellipse(windowWidth/2,windowHeight/2-45,xol);

let xolo = map(noise(xoloff),0,1,200,350);
ellipse(windowWidth/2,windowHeight/2-45,xolo);

xoloff += 0.003;





start+=inc;
let yoff=start;

let y=noise(yoff)*height; //bådplacering
let xboat = map(noise(xoloff),0,1,windowWidth/2-400,windowWidth/2+100);
xboat += 0.0001;

beginShape();
noStroke();
for (var x=0;x<width;x++){
yoff+=0.0008; //bølgehøjde
let y=noise(yoff)*height/2; //"placering"
fill(68,10,127,40)
vertex(x,y);
}
endShape();

beginShape();
noStroke();
for (var x=0;x<width;x++){
yoff+=0.0008; //bølgehøjde
let y=noise(yoff)*height; //"placering"
fill(68,10,127,100)
vertex(x,y);
}
endShape();

beginShape();
noStroke();
for (var x=0;x<width;x++){
yoff+=0.0008; //bølgehøjde
let y=noise(yoff)*height; //"placering"
fill(68,10,127,100)
vertex(x,y);
}
endShape();

beginShape();
noStroke();
for (var x=0;x<width;x++){
yoff+=0.0008; //bølgehøjde
let y=noise(yoff)*height; //"placering"
fill(68,10,127,40)
vertex(x,y);
}
endShape();

beginShape();
noStroke();
for (var x=0;x<width;x++){
yoff+=0.0006; //bølgehøjde
let y=noise(yoff)*height; //"placering"
fill(68,10,127,150)
vertex(x,y);
}
endShape();

//boat
fill(219,78,0)
triangle(xboat,y+20,xboat+30,y+50,xboat+100,y+20)
fill(255,211,0,235)
triangle(xboat+5,y+12,xboat+30,y-50,xboat+92,y+10)
fill(240)
rect(xboat+30,y-50,2,70)

beginShape();
noStroke();
for (var x=0;x<width;x++){
yoff+=0.0004; //bølgehøjde
let y=noise(yoff)*height*1.2; //"placering"
fill(68,10,127,50)
vertex(x,y);
}
endShape();

beginShape();
noStroke();
for (var x=0;x<width;x++){
yoff+=0.0004; //bølgehøjde
let y=noise(yoff)*height*1.4; //"placering"
fill(68,10,127,200)
vertex(x,y);
}
endShape();

//borders
fill(255)
rect(0,windowHeight/1.2,windowWidth,windowHeight)
rect(0,0,windowWidth,windowHeight/8)

}
