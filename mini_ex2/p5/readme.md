Mini_ex2


![PNG of boat](FF.PNG)


https://gl.githack.com/MortenKry/ap2019---morten/raw/master/mini_ex2/p5/empty-example/index.html


For this mini_ex I created two emojis with simple syntaxes such as text() and rect(). It is supposed to immitate a certain meme and serves as a comment on how emojis, if not reduced to their simplest form (and maybe not even then), will always have different connotations and representation validity for different individuals and groups. 


As time moves on, emojis evolve in many directions. The sheer number of emojis expand and the variety increases for better and for worse. Recently, Facebook came under scrutiny for their non-inclusive emojis, with the problem being that the emojis had mainly the standard yellow skin tone or a light, white skin tone with little to no option for other representation of races than caucassian. Moreover, emojis of people with disabilities were severely lacking or absent. Facebook, together with many other platforms using emojis, has now included a wider variety of representation in their emojis and keeps announcing and releasing new emojis continually, now with a critical eye for any misrepresentation, bigotry and the like. 


While the emojis I have made do not quite reflect this global discussion, I enjoyed the process and kept it simple all the while trying to keep the context of the read literature in mind. 


