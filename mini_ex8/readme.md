RUNME: https://glcdn.githack.com/NicolaiRieder/ap/raw/master/Mini_ex8/p5/empty-example/index.html


JSON's: https://gitlab.com/NicolaiRieder/ap/tree/master/Mini_ex8/p5/empty-example/assets


Sourcecode: https://gitlab.com/NicolaiRieder/ap/blob/master/Mini_ex8/p5/empty-example/sketch.js

![PNG of doge](doge.PNG)

This Week's mini_ex was made by everyone in Group 3. The program was inspired by the simple format of the name generator of which there exists a wide variety of on the internet. As often is with group-projects we had a couple of considerations up in the air, but once we had settled on the simple name generator our primary consideration was to make the program interactive. Our most prominent idea was to simply let the user write either their date of birth or name and then let the program generate a dog-name from a set of rules or, more conveniently, at random. 
We chose to keep it simple as we ran into a wall when we tried call more than one random word from the different JSON's. With our united effort (supplemented by Ann Karring) we managed to fight our way through it.


The collaborative process in general can go many ways. I believe it ran rather smoothly for us. The research of problems with the sourcecode was definitely faster, but it could pose troublesome to come to an agreement on how the program should pan out. 


In retrospect, it would have fitted the assignment more to work with the relationship between the code and language. As for now the code is "merely" code - that is with no designed intention other than that of the RUNME. A proposal we discussed was to present our sourcecode into the theme of dogs, perhaps with puns and more. Partly due to time contrains, this did not happen. Code and language are interrelated and I think it would have been interesting to create code which would serve as a product beyond its RUNME. 

